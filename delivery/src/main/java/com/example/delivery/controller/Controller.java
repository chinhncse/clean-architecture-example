package com.example.delivery.controller;

import com.example.entity.Student;
import com.example.request.StudentRequest;
import com.example.response.StudentResponse;
import com.example.usecase.StudentUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class Controller {

    @Autowired
    private StudentUseCase studentUseCase;

    @GetMapping("/students")
    public StudentResponse getAllStudents(){
        List<Student> students = studentUseCase.getAllStudents();
        StudentResponse response = new StudentResponse();
        response.setMessage("Error while getting all students");
        response.setStatusCode("500");
        if(students != null && students.size() > 0){
            response.setObjects(students);
            response.setStatusCode("200");
            response.setMessage("Success");
        }
        return response;
    }

    @PostMapping("/students")
    public StudentResponse createStudent(@RequestBody StudentRequest request){
        StudentResponse response = new StudentResponse();
        try{
            studentUseCase.create(new Student(request.getEmail(), request.getName()));
            response.setStatusCode("200");
            response.setMessage("Success");
        }catch (Exception e){
            response.setMessage("Error while create new student");
            response.setStatusCode("500");
            e.printStackTrace();
        }
        return response;
    }


}
