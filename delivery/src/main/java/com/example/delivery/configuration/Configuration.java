package com.example.delivery.configuration;

import com.example.gateway.StudentGateWay;
import com.example.impl.StudentUseCaseImpl;
import com.example.utils.StudentRepositoryConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;

@org.springframework.context.annotation.Configuration
public class Configuration {

    @Autowired
    private StudentGateWay studentGateWay;

    @Bean
    public StudentUseCaseImpl studentUseCase() {
        return new StudentUseCaseImpl(studentGateWay);
    }

    @Bean
    public StudentRepositoryConverter studentRepositoryConverter() {
        return new StudentRepositoryConverter();
    }


}
