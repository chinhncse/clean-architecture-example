package com.example.base;

import java.util.List;

public class AbstractResponse<ID, T> {

    private String message;
    private String statusCode;
    T Object;
    List<T> objects;

    public AbstractResponse() {
    }

    public AbstractResponse(String message, String statusCode, T object) {
        this.message = message;
        this.statusCode = statusCode;
        Object = object;
    }

    public AbstractResponse(String message, String statusCode, List<T> objects) {
        this.message = message;
        this.statusCode = statusCode;
        this.objects = objects;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public T getObject() {
        return Object;
    }

    public void setObject(T object) {
        Object = object;
    }

    public List<T> getObjects() {
        return objects;
    }

    public void setObjects(List<T> objects) {
        this.objects = objects;
    }
}
