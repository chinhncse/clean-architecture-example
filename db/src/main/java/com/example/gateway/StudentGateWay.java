package com.example.gateway;

import com.example.entity.Student;

import java.util.List;
import java.util.Optional;

public interface StudentGateWay {

    void create(Student student);

    List<Student> getAllStudents();

    Optional<Student> findByEmail(String email);

}
