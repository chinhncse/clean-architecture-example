package com.example.response;

import com.example.base.AbstractResponse;
import com.example.entity.Student;

public class StudentResponse extends AbstractResponse<Integer, Student> {

    public StudentResponse(){
        super();
    }
}
