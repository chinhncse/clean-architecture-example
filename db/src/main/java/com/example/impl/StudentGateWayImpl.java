package com.example.impl;

import com.example.entity.Student;
import com.example.gateway.StudentGateWay;
import com.example.repository.StudentRepository;
import com.example.utils.StudentRepositoryConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class StudentGateWayImpl implements StudentGateWay {

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private StudentRepositoryConverter studentRepositoryConverter;

    @Override
    public void create(Student student) {
        studentRepository.save(studentRepositoryConverter.mapToTable(student));
    }

    @Override
    public List<Student> getAllStudents() {
        return studentRepository.findAll().stream().map(studentEntity -> studentRepositoryConverter.mapToEntity(studentEntity)).collect(Collectors.toList());
    }

    @Override
    public Optional<Student> findByEmail(String email) {
        return studentRepository.findByEmail(email).map(studentEntity -> studentRepositoryConverter.mapToEntity(studentEntity));
    }
}
