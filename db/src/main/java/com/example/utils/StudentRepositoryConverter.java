package com.example.utils;

import com.example.entities.StudentEntity;
import com.example.entity.Student;

public class StudentRepositoryConverter implements RepositoryConverter<StudentEntity, Student> {
    @Override
    public StudentEntity mapToTable(Student persistenceObject) {
        return new StudentEntity(persistenceObject.getId(), persistenceObject.getEmail(), persistenceObject.getName());
    }

    @Override
    public Student mapToEntity(StudentEntity tableObject) {
        Student student = Student.builder()
                .id(tableObject.getId())
                .email(tableObject.getEmail())
                .name(tableObject.getName()).build();
        return student;
    }
}
