package com.example.utils;

import java.io.Serializable;

public interface RepositoryConverter<T extends Serializable, P extends  Serializable> {

    T mapToTable(P persistenceObject);

    P mapToEntity(T tableObject);
}
