package com.example.entity;

import java.io.Serializable;

public class Student implements Serializable {

    private int id;
    private String email;
    private String name;


    private Student() {
    }

    public Student(String email, String name) {
        this.email = email;
        this.name = name;

    }

    public Student(int id, String email, String name) {
        this.id = id;
        this.email = email;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static StudentBuilder builder() {
        return new StudentBuilder();
    }

    public static class StudentBuilder {
        private int id;
        private String email;
        private String name;
        private String phone;
        private String address;


        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        StudentBuilder() {
        }

        public StudentBuilder id(final int id) {
            this.id = id;
            return this;
        }

        public StudentBuilder email(final String email) {
            this.email = email;
            return this;
        }

        public StudentBuilder name(final String name) {
            this.name = name;
            return this;
        }

        public Student build() {
            return new Student(id, email, name);
        }
    }
}
