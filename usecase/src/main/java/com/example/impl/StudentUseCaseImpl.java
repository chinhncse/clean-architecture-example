package com.example.impl;

import com.example.entity.Student;
import com.example.gateway.StudentGateWay;
import com.example.usecase.StudentUseCase;

import java.util.List;
import java.util.Optional;

public class StudentUseCaseImpl implements StudentUseCase {

    private StudentGateWay studentGateWay;

    public StudentUseCaseImpl(StudentGateWay studentGateWay) {
        this.studentGateWay = studentGateWay;
    }

    @Override
    public void create(Student student) {
        //TODO check validation
        studentGateWay.create(student);
    }

    @Override
    public List<Student> getAllStudents() {
        return studentGateWay.getAllStudents();
    }

    @Override
    public Optional<Student> findByEmail(String email) {
        return studentGateWay.findByEmail(email);
    }
}
