package com.example.usecase;

import com.example.entity.Student;

import java.util.List;
import java.util.Optional;

public interface StudentUseCase {

    public void create(Student student);

    public List<Student> getAllStudents();

    public Optional<Student> findByEmail(String email);


}
